tree grammar TExpr1;

options {
	tokenVocab=Expr;

	ASTLabelType=CommonTree;
    superClass=MyTreeParser;
}

@header {
package tb.antlr.interpreter;
}

//prog    : (e=expr {drukuj ($e.text + " = " + $e.out.toString());})* ;
prog    : (expr)*;

expr returns [Integer out]
	      : ^(PLUS  e1=expr e2=expr) {$out = $e1.out + $e2.out;}
        | ^(MINUS e1=expr e2=expr) {$out = $e1.out - $e2.out;}
        | ^(MUL   e1=expr e2=expr) {$out = $e1.out * $e2.out;}
        | ^(DIV   e1=expr e2=expr) {$out = divide($e1.out, $e2.out);}
        | ^(POW   e1=expr e2=expr) {$out = power($e1.out,$e2.out);}
        | ^(PRINT e1=expr)         {print($e1.out);}
        | ^(AND   e1=expr e2=expr) {$out = $e1.out & $e2.out;}
        | ^(OR    e1=expr e2=expr) {$out = $e1.out | $e2.out;}
        | ^(XOR   e1=expr e2=expr) {$out = $e1.out ^ $e2.out;}
        | ^(VAR ID)                 {varLoc.newSymbol($ID.text);}
        | ^(PODST i1=ID   e2=expr) {varLoc.setSymbol($i1.text, $e2.out);}
        | INT                      {$out = getInt($INT.text);}
        | ID                       {$out = varLoc.getSymbol($ID.text);} 
        | O_BRACKET {varLoc.enterScope();}
        | C_BRACKET {varLoc.leaveScope();}
        ;
