grammar Expr;

options {
  output=AST;
  ASTLabelType=CommonTree;
}

@header {
package tb.antlr;
}

@lexer::header {
package tb.antlr;
}

prog
    : (stat | blok)+ EOF!;

blok : O_BRACKET^ (stat | blok)* C_BRACKET!
    ;

stat
    : expr NL -> expr

    | VAR ID (PODST expr)? NL -> ^(VAR ID) ^(PODST ID expr)?
    | PRINT expr NL -> ^(PRINT expr)
//    | VAR ID NL -> ^(VAR ID)
    | ID PODST expr NL -> ^(PODST ID expr)
    | if_stat NL -> if_stat
    
    | NL ->
    ;

if_stat
    : IF^ expr THEN! (expr) (ELSE! (expr))?
    ;

expr
    : multExpr
      ( PLUS^ multExpr
      | MINUS^ multExpr
      )*
    ;

multExpr
    : bitExpr
      ( MUL^ bitExpr
      | DIV^ bitExpr
      | POW^ bitExpr
      )*
    ;

bitExpr
    : atom
      ( AND^ atom
      | OR^ atom 
      | XOR^ atom
      )*
      ;

atom
    : INT
    | ID
    | LP! expr RP!
    ;

VAR :'var';

IF : 'if';

ELSE : 'else';

THEN : 'then';

PRINT : 'print';

ID : ('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'0'..'9'|'_')*;

INT : '0'..'9'+;

NL : '\r'? '\n' ;

WS : (' ' | '\t')+ {$channel = HIDDEN;} ;


LP
	:	'('
	;

RP
	:	')'
	;

PODST
	:	'='
	;

PLUS
	:	'+'
	;

MINUS
	:	'-'
	;

MUL
	:	'*'
	;

DIV
	:	'/'
	;
	
POW
  : '^^'
  ;

AND
  : '&'
  ;
  
OR 
  : '|'
  ;
   
XOR
  : '^'
  ;
  
O_BRACKET
  : '{'
  ;
  
C_BRACKET
  : '}'
  ;